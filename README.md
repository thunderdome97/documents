# Documentatie stage systeem

## Bestanden

| Document             | Beschrijving                          |
| -------------------- | ------------------------------------- |
| Plan van Aanpak      | De omschrijving van het project       |
| Project Eisen        | De eisen en wensen gaande het project |
| Interview Vragen     | De vragen voor bij de interviews      |
| Interview Uitwerking | De interviews uitgewerkt              |

- Word documenten in `/Word`
- PDF documenten in `/PDF`
