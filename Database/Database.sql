CREATE TABLE `users` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `first_name` varchar(255),
  `middle_name` varchar(255),
  `last_name` varchar(255),
  `student_id` int(11),
  `crebo` int(11),
  `email` varchar(255),
  `password` varchar(255),
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `application` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(11),
  `company_internship_id` int(11),
  `start_date` timestamp,
  `end_date` timestamp,
  `status_slug` varchar(255),
  `validated` boolean,
  `validated_by` int(11),
  `validated_date` int(11),
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `tags` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `item_tags` (
  `model_type` varchar(255),
  `model_id` int(11),
  `tag_id` int(11)
);

CREATE TABLE `internships` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `application_id` int(11),
  `status_slug` varchar(255),
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `pok_status` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `application_id` int(11),
  `status_slug` varchar(255),
  `validated` boolean,
  `validated_by` int(11),
  `validated_date` timestamp,
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `internship_hours` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `internship_id` int(11),
  `hours` int(11),
  `description` varchar(255),
  `date` timestamp,
  `status` int(11),
  `validated` boolean,
  `validated_by` int(11),
  `validated_date` timestamp,
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `companies` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `description` varchar(255),
  `valitated` boolean,
  `public` boolean,
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `user_companies` (
  `user_id` int(11),
  `company_id` int(11)
);

CREATE TABLE `company_internships` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `company_id` int(11),
  `name` varchar(255),
  `description` varchar(255),
  `date_from` timestamp,
  `date_to` timestamp,
  `available_spots` int(11),
  `crebo` int(11),
  `visible` boolean,
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `uploads` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(11),
  `file_title` varchar(255),
  `file_name` varchar(255),
  `file_location` varchar(255),
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `roles` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `slug` varchar(255),
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `permissions` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `slug` varchar(255),
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `user_roles` (
  `role_id` int(11),
  `user_id` int(11)
);

CREATE TABLE `role_permissions` (
  `permission_id` int(11),
  `role_id` int(11)
);

CREATE TABLE `educations` (
  `crebo` int(11) PRIMARY KEY,
  `name` varchar(255),
  `slug` varchar(255),
  `created_at` timestamp,
  `deleted_at` timestamp,
  `updated_at` timestamp
);

CREATE TABLE `statuses` (
  `slug` varchar(255) PRIMARY KEY,
  `model` varchar(255),
  `name` varchar(255),
  `color` varchar(255)
);

ALTER TABLE `user_roles` ADD FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

ALTER TABLE `user_roles` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `users` ADD FOREIGN KEY (`crebo`) REFERENCES `educations` (`crebo`);

ALTER TABLE `role_permissions` ADD FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`);

ALTER TABLE `role_permissions` ADD FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

ALTER TABLE `user_companies` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `user_companies` ADD FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

ALTER TABLE `company_internships` ADD FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

ALTER TABLE `company_internships` ADD FOREIGN KEY (`crebo`) REFERENCES `educations` (`crebo`);

ALTER TABLE `application` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `application` ADD FOREIGN KEY (`company_internship_id`) REFERENCES `company_internships` (`id`);

ALTER TABLE `pok_status` ADD FOREIGN KEY (`application_id`) REFERENCES `application` (`id`);

ALTER TABLE `pok_status` ADD FOREIGN KEY (`status_slug`) REFERENCES `statuses` (`slug`);

ALTER TABLE `pok_status` ADD FOREIGN KEY (`validated_by`) REFERENCES `users` (`id`);

ALTER TABLE `internship_hours` ADD FOREIGN KEY (`internship_id`) REFERENCES `internships` (`id`);

ALTER TABLE `internship_hours` ADD FOREIGN KEY (`validated_by`) REFERENCES `users` (`id`);

ALTER TABLE `internships` ADD FOREIGN KEY (`application_id`) REFERENCES `application` (`id`);

ALTER TABLE `internships` ADD FOREIGN KEY (`status_slug`) REFERENCES `statuses` (`slug`);

ALTER TABLE `application` ADD FOREIGN KEY (`status_slug`) REFERENCES `statuses` (`slug`);

ALTER TABLE `item_tags` ADD FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);
