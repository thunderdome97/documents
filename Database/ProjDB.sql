-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: localhost    Database: Proj
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `application` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `company_internship_id` int DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `status_slug` varchar(255) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  `validated_by` int DEFAULT NULL,
  `validated_date` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `company_internship_id` (`company_internship_id`),
  KEY `status_slug` (`status_slug`),
  CONSTRAINT `application_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `application_ibfk_2` FOREIGN KEY (`company_internship_id`) REFERENCES `company_internships` (`id`),
  CONSTRAINT `application_ibfk_3` FOREIGN KEY (`status_slug`) REFERENCES `statuses` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `companies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `valitated` tinyint(1) DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'ict-bh','ict Beheer bedrijf',1,1,'2020-01-27 16:40:21',NULL,NULL),(2,'ict-ao1','ao bedrijf',1,1,'2020-01-27 16:40:21',NULL,NULL),(3,'ict-ao2','applicatie ontwikkelaar',1,1,'2020-01-27 16:40:21',NULL,NULL),(4,'ict-mo1','media ontw',1,1,'2020-01-27 16:40:21',NULL,NULL),(5,'ict-nb1','netwerk bh',1,1,'2020-01-27 16:40:21',NULL,NULL),(6,'ict-nv2','Ict nivau',1,1,'2020-01-27 16:40:21',NULL,NULL),(7,'ict-nv3','ict niv',1,1,'2020-01-27 16:40:21',NULL,NULL),(8,'ict-nv4','ict niveau',1,1,'2020-01-27 16:40:21',NULL,NULL),(9,'ict-ao3','applicatie drie',1,1,'2020-01-27 16:40:21',NULL,NULL),(10,'ict-ao4','applicatie vier',1,1,'2020-01-27 16:40:21',NULL,NULL);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_internships`
--

DROP TABLE IF EXISTS `company_internships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_internships` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `available_spots` int DEFAULT NULL,
  `crebo` int DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `crebo` (`crebo`),
  CONSTRAINT `company_internships_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `company_internships_ibfk_2` FOREIGN KEY (`crebo`) REFERENCES `educations` (`crebo`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_internships`
--

LOCK TABLES `company_internships` WRITE;
/*!40000 ALTER TABLE `company_internships` DISABLE KEYS */;
INSERT INTO `company_internships` VALUES (1,1,'ao stage!','sdefrgthgfde','2020-01-27 16:41:40','2020-05-31 22:00:00',2,1,1,'2020-01-27 16:42:00',NULL,NULL),(2,1,'ao stage!','sdefrgthgfde','2020-01-27 16:41:40','2020-05-31 22:00:00',2,1,1,'2020-01-27 16:42:00',NULL,NULL),(3,1,'ao stage!','sdefrgthgfde','2020-01-27 16:41:40','2020-05-31 22:00:00',2,1,1,'2020-01-27 16:42:00',NULL,NULL),(4,1,'ao stage!','sdefrgthgfde','2020-01-27 16:41:40','2020-05-31 22:00:00',2,1,1,'2020-01-27 16:42:00',NULL,NULL),(5,1,'ao stage!','sdefrgthgfde','2020-01-27 16:41:40','2020-05-31 22:00:00',2,1,1,'2020-01-27 16:42:00',NULL,NULL),(6,1,'ao stage!','sdefrgthgfde','2020-01-27 16:41:40','2020-05-31 22:00:00',2,1,1,'2020-01-27 16:42:00',NULL,NULL),(7,2,'bh stage','bheer','2020-01-27 16:41:40','2020-05-31 22:00:00',1,1,1,'2020-01-27 16:42:00',NULL,NULL),(8,2,'bh stage','bheer','2020-01-27 16:41:40','2020-05-31 22:00:00',4,1,1,'2020-01-27 16:42:00',NULL,NULL),(9,3,'bh stage','bheer','2020-01-27 16:41:40','2020-05-31 22:00:00',3,1,1,'2020-01-27 16:42:00',NULL,NULL),(10,3,'bh stage','bheer','2020-01-27 16:41:40','2020-05-31 22:00:00',1,1,1,'2020-01-27 16:42:00',NULL,NULL),(11,4,'bh stage','bheer','2020-01-27 16:41:40','2020-05-31 22:00:00',5,1,1,'2020-01-27 16:42:00',NULL,NULL),(12,4,'Sch stage','schilder','2020-01-27 16:41:40','2020-05-31 22:00:00',1,1,1,'2020-01-27 16:42:00',NULL,NULL),(13,5,'Sch stage','schilder','2020-01-27 16:41:40','2020-05-31 22:00:00',2,1,1,'2020-01-27 16:42:00',NULL,NULL),(14,5,'Sch stage','schilder','2020-01-27 16:41:40','2020-05-31 22:00:00',17,1,1,'2020-01-27 16:42:00',NULL,NULL),(15,6,'Sch stage','schilder','2020-01-27 16:41:40','2020-05-31 22:00:00',1,1,1,'2020-01-27 16:42:00',NULL,NULL);
/*!40000 ALTER TABLE `company_internships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `educations`
--

DROP TABLE IF EXISTS `educations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `educations` (
  `crebo` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`crebo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `educations`
--

LOCK TABLES `educations` WRITE;
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
INSERT INTO `educations` VALUES (1,'ao','Applicatieontwikkelaar','2020-01-27 16:39:01',NULL,NULL),(2,'bh','ICT-Beheer','2020-01-27 16:39:01',NULL,NULL);
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `internship_hours`
--

DROP TABLE IF EXISTS `internship_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `internship_hours` (
  `id` int NOT NULL AUTO_INCREMENT,
  `internship_id` int DEFAULT NULL,
  `hours` int DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `status` int DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  `validated_by` int DEFAULT NULL,
  `validated_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `internship_id` (`internship_id`),
  KEY `validated_by` (`validated_by`),
  CONSTRAINT `internship_hours_ibfk_1` FOREIGN KEY (`internship_id`) REFERENCES `internships` (`id`),
  CONSTRAINT `internship_hours_ibfk_2` FOREIGN KEY (`validated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `internship_hours`
--

LOCK TABLES `internship_hours` WRITE;
/*!40000 ALTER TABLE `internship_hours` DISABLE KEYS */;
/*!40000 ALTER TABLE `internship_hours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `internships`
--

DROP TABLE IF EXISTS `internships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `internships` (
  `id` int NOT NULL AUTO_INCREMENT,
  `application_id` int DEFAULT NULL,
  `status_slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `status_slug` (`status_slug`),
  CONSTRAINT `internships_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`),
  CONSTRAINT `internships_ibfk_2` FOREIGN KEY (`status_slug`) REFERENCES `statuses` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `internships`
--

LOCK TABLES `internships` WRITE;
/*!40000 ALTER TABLE `internships` DISABLE KEYS */;
/*!40000 ALTER TABLE `internships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_tags`
--

DROP TABLE IF EXISTS `item_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_tags` (
  `model_type` varchar(255) DEFAULT NULL,
  `model_id` int DEFAULT NULL,
  `tag_id` int DEFAULT NULL,
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `item_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_tags`
--

LOCK TABLES `item_tags` WRITE;
/*!40000 ALTER TABLE `item_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pok_status`
--

DROP TABLE IF EXISTS `pok_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pok_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `application_id` int DEFAULT NULL,
  `status_slug` varchar(255) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  `validated_by` int DEFAULT NULL,
  `validated_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `status_slug` (`status_slug`),
  KEY `validated_by` (`validated_by`),
  CONSTRAINT `pok_status_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`),
  CONSTRAINT `pok_status_ibfk_2` FOREIGN KEY (`status_slug`) REFERENCES `statuses` (`slug`),
  CONSTRAINT `pok_status_ibfk_3` FOREIGN KEY (`validated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pok_status`
--

LOCK TABLES `pok_status` WRITE;
/*!40000 ALTER TABLE `pok_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `pok_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permissions`
--

DROP TABLE IF EXISTS `role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_permissions` (
  `permission_id` int DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  KEY `permission_id` (`permission_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `role_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `role_permissions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permissions`
--

LOCK TABLES `role_permissions` WRITE;
/*!40000 ALTER TABLE `role_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'user','Leerling','2020-01-27 16:37:33',NULL,NULL),(2,'compnay','Bedrijf','2020-01-27 16:37:33',NULL,NULL),(3,'mentor','Mentor','2020-01-27 16:37:33',NULL,NULL),(4,'admin','Administrator','2020-01-27 16:37:33',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statuses` (
  `slug` varchar(255) NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES ('amm','amm','maa','looo'),('bo','mo','po','zo'),('do','re','mi','fa');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (16,'JAva','2020-07-09 22:00:00',NULL,NULL),(18,'Moa','2020-07-09 22:00:00',NULL,NULL),(19,'php','2020-07-09 22:00:00',NULL,NULL),(20,'C','2020-07-09 22:00:00',NULL,NULL);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `uploads` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `file_title` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_location` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` VALUES (1,11,'a','a','yes','2020-07-09 22:00:00',NULL,NULL),(2,22,'b','b','no','2020-07-09 22:00:00',NULL,NULL);
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_companies`
--

DROP TABLE IF EXISTS `user_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_companies` (
  `user_id` int DEFAULT '0',
  `company_id` int DEFAULT '0',
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `user_companies_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_companies_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_companies`
--

LOCK TABLES `user_companies` WRITE;
/*!40000 ALTER TABLE `user_companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_hash`
--

DROP TABLE IF EXISTS `user_hash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_hash` (
  `id` int NOT NULL,
  `userId` int DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_hash`
--

LOCK TABLES `user_hash` WRITE;
/*!40000 ALTER TABLE `user_hash` DISABLE KEYS */;
INSERT INTO `user_hash` VALUES (1,1,'stu','2020-01-10 00:00:00'),(2,2,'stu','2020-07-10 00:00:00'),(3,3,'ler','2020-07-10 00:00:00'),(4,4,'ler','2020-07-10 00:00:00'),(5,5,'bd','2020-07-10 00:00:00'),(6,6,'bd','2020-07-10 00:00:00');
/*!40000 ALTER TABLE `user_hash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `role_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `crebo` int DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `crebo` (`crebo`,`id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Student','aaaaa','aaaaa',11,1,'bjorn@local','123','2020-07-09 22:00:00',NULL,NULL),(2,'Stedent','Bodje','Potje',22,1,'Rik@no','321','2020-07-09 22:00:00',NULL,NULL),(3,'Leraar','hodje','knotje',NULL,NULL,'Bmail@nono','6','2020-07-09 22:00:00',NULL,NULL),(4,'Raarleer','meneer','peer',NULL,NULL,'Gmail@yes','7','2020-07-09 22:00:00',NULL,NULL),(5,'Brrijf','Schijf','Mand',NULL,NULL,'Belangrijk@yahoo','9000','2020-07-09 22:00:00',NULL,NULL),(6,'besdrijf','lijf','Hond',NULL,NULL,'Yahoo@filter','hebknie','2020-07-09 22:00:00',NULL,NULL),(7,'harry','student','voor altijd',33,231,'Yao@filter','dasd','2020-07-09 22:00:00',NULL,NULL),(8,'barry','student','voor altijd',44,231,'Yaoo@filter','fdaf','2020-07-09 22:00:00',NULL,NULL),(9,'garry','student','voor altijd',55,222,'fdnjasn@sdjfna','qe','2020-07-09 22:00:00',NULL,NULL),(10,'zarry','student','voor altijd',66,111,'geen@mail','vcx','2020-07-09 22:00:00',NULL,NULL),(11,'mary','student','voor altijd',77,532,'hi@mi','e2r','2020-07-09 22:00:00',NULL,NULL),(12,'parry','student','voor altijd',88,335,'Bla@BLa','sada','2020-07-09 22:00:00',NULL,NULL),(13,'darry','student','voor altijd',89,123,'www@www','ccas','2020-07-09 22:00:00',NULL,NULL),(14,'varry','student','voor altijd',78,213,'Oint@j','fr3t','2020-07-09 22:00:00',NULL,NULL),(15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-28  0:14:55
